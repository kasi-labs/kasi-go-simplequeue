package simplequeue

import (
	"gitlab.com/kasi-labs/kasi-go-queue/queue"

	"reflect"
	"sync"
)

// SimpleQueue implement simple Queue system
type SimpleQueue struct {
	Queue []interface{}
	mu    sync.RWMutex // For increment / decrement prevent reads and writes
}

func NewSimpleQueue() *SimpleQueue {
	return &SimpleQueue{
		Queue: make([]interface{}, 0),
		mu:    sync.RWMutex{},
	}
}

// Push value to Queue
func (q *SimpleQueue) Push(value interface{}) error {
	q.mu.RLock()
	defer q.mu.RUnlock()

	rv := reflect.ValueOf(value)

	if rv.Type().Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	rvCache := reflect.New(rv.Type())
	rvCache.Elem().Set(rv)
	item := rvCache.Elem().Interface()
	q.Queue = append(q.Queue, item)

	return nil
}

// Pop value from Queue
func (q *SimpleQueue) Pop(ptrValue interface{}) error {
	q.mu.RLock()
	defer q.mu.RUnlock()

	if len(q.Queue) == 0 {
		return queue.ErrQueueIsEmpty
	}

	item := q.Queue[0]
	q.Queue = q.Queue[1:]

	v := reflect.ValueOf(ptrValue)
	if v.Type().Kind() == reflect.Ptr && v.Elem().CanSet() {
		v.Elem().Set(reflect.ValueOf(item))
		return nil
	}

	return nil
}

func (q *SimpleQueue) Close() error {
	return nil
}
