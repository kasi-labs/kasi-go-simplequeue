package simplequeue

import (
	"gitlab.com/kasi-labs/kasi-go-queue/queue"
	"gitlab.com/kasi-labs/kasi-go-queue/queue/unit_test"
	"testing"
)

var newFactory= func() queue.NewFactory {
	simpleQueue := NewSimpleQueue()
	return queue.NewFactory {
		NewPopper: func() queue.Popper  { return simpleQueue },
		NewPusher: func() queue.Pusher  { return simpleQueue },
	}
}

// Test typical Queue push/pop
func TestSimpleQueue_PushPop(t *testing.T) {
	unit_test.TestTropicalPushPop(t, newFactory())
}
